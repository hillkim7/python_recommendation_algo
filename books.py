import random
import pandas as pd
import numpy as np
import shutil, os

# 시그모이드 함수
def sigmoid(x):
    sig = 1 / (1 + np.exp(-x))
    return sig

# 시그모이드 역함수
def sigmoid_reversed(x):
    sig_reversed = np.log(x/(1-x))
    return sig_reversed

class Books:
    divisions = ('자연과학계열', '의약계열', '공학계열')
    subdivisions = [
        ['기초과학', '농림수산', '생활과학'],
        ['의학', '약학', '간호', '보건'],
        ['교통', '기계', '전기', '토목', '화학']
    ]

    def __init__(self, csv_path: str):
        self.department_map = {}
        self.department_map['기초과학'] = ['대기과학과', '물리학과', '미생물학과', '바이오동물학과', '생명공학과', '수학과', '식량자원학과']
        self.department_map['농림수산'] = ['동물자원학과', '수산학과', '식물자원학과']
        self.department_map['생활과학'] = ['생활문화과', '외식조리과']
        self.department_map['의학'] = ['수의학과', '의예과', '의료정보공학과', '의용공학과']
        self.department_map['약학'] = ['약학부']
        self.department_map['간호'] = ['간호과', '간호학과']
        self.department_map['보건'] = ['물리치료과', '방사선과', '보건관리학과', '보건행정과', '산업안전과', '소방안전관리과', '예술치료학과', '응급구조과', '임상병리과', '재활학과', '치기공과']
        self.department_map['전기'] = ['게임공학과', '게임콘텐츠과', '국방기술과']
        self.department_map['화학'] = ['고분자공학과', '공업화학과', '광학공학과', '금속공학과', '금형설계과', '나노공학과', '식품공학과']
        self.department_map['기계'] = ['기계공학과', 'IT융합학과']
        self.department_map['토목'] = ['건설정보과', '건축설비과', '건축학과', '도시건설과', '물류시스템공학과']
        self.department_map['교통'] = ['교통공학과', '해양공학과']
        self.csv_path = csv_path
        self.books = pd.read_csv(csv_path, encoding='euc-kr')
        self.department_corr = pd.read_csv('참고자료/학과 유사도.csv',encoding='euc-kr')
        self.department_corr.columns = ['학과'] + list(self.department_corr.columns[1:])
        self.department_corr.set_index('학과', inplace=True)
        #print('department_corr', self.department_corr.head())
        self.book_corr = pd.read_csv('참고자료/학과 도서 유사도.csv', encoding='euc-kr')
        self.book_corr.columns = ['학과'] + list(self.book_corr.columns[1:])
        self.book_corr.set_index('학과', inplace=True)
        self.all_books = self.book_corr.columns.tolist()
        self.department_names = self.book_corr.index.tolist()
        try:
            self.load_tables()
        except Exception as e:
            print("Error: ", e)
            # table 읽기 실패시 초기 값 파일 생성
            self.set_tables_with_defaults()
            self.save_tables()
        #print('book_corr', self.book_corr.head())

    def get_book(self, bookid):
        """ 다음과 같은 책 정보 반환
        id                                                         0
        subject                                                 교양물리
        title                                어떻게 물리학을 사랑하지 않을 수 있을까?
        publish                                                   윌북
        author                                                짐 알칼릴리
        image      http://image.kyobobook.co.kr/images/book/large...
        info       『어떻게 물리학을 사랑하지 않을 수 있을까?』는 대중과 과학을 연결하는 과학 커뮤니
        """
        # book =  self.books.iloc[bookid]
        # return book.to_dict() 
        df2 = self.books.query(f'id == {bookid}')
        obj_list = df2.to_dict('records') 
        if len(obj_list) > 0:
            return obj_list[0]

    def find_book(self, title):
        """
        Object 형태의 제목에 일치되는 책 정보 검색한다.
        """
        df2 = self.books.query(f'title == "{title}"')
        obj_list = df2.to_dict('records') 
        if len(obj_list) > 0:
            return obj_list[0]

    def get_department_names(self, subdivision_name):
        departments = self.department_map[subdivision_name]
        # 책 상관관계에 없는 학과로 데이터 조회시 오류가 남으로
        # 책 상관관계에 존재하는 학과만 반환
        valid_departments = [d for d in departments if d in self.book_corr.index]
        return valid_departments

    def suggest_top_books(self, department, n):
        """특정 세부 학과 계열의 책을 n개 추천한다."""
        # 특정 확과 데이터만 선택
        # 학과가 데이터에 없는 경우 오류 남으로 있는것만 검색
        print(f'suggest_top_books: department={department}')
        if department in self.book_corr.index:
            selected = self.book_corr.loc[department]
        else:
            return []
        #print(f'selected: {type(selected)} {len(selected)}')
        # 선택된 데이터의 평균을 구한후 점수가 높은 데이터 n개 선택
        ##book_score = selected.mean().sort_values(ascending=False).iloc[0:n]
        book_score = selected.sort_values(ascending=False).iloc[0:n]
        # 책 타이틀을 빼냄
        book_titles = book_score.index.tolist()
        # 유사도를 빼냄
        scores = book_score.values.tolist()
        books = []
        for i in range(len(book_titles)):
            book = self.find_book(book_titles[i])
            book['score'] = scores[i]
            books.append(book)
        return books

    def update_book_score(self, division_name, subdivision_name, department, book, score):
        lr = 0.1 # 점수반영률
        df = self.book_corr
        book_title = book['title']
        print('score change:')
        print(df[book_title])
        print('------------->')
        if score:
            df[book_title] = sigmoid(sigmoid_reversed(df[book_title]) + lr * df[book_title]* self.department_corr[department] )
        else:
            df[book_title] = sigmoid(sigmoid_reversed(df[book_title]) - lr * df[book_title]* self.department_corr[department] )
        print(df[book_title])
        self.save_score_history(division_name, subdivision_name, department, book_title, score)
        self.save_to_csv()

    def get_score_data_frame(self):
        posteriors = [] # 사후확률. 최종 추천 점수
        print(f'len(all_books)={len(self.all_books)} len(self.department_names)={len(self.department_names)}')
        for i in range(len(self.all_books)): #데이터베이스의 모든 책에 대해 사후 확률 구함
            book_title = self.all_books[i]
            d = self.book_corr[book_title] # 각 도서에 대한 모든 학과의 유사도
            d = d.tolist() #list 형식 변환
            ''''리스트에 추가. 왜냐하면 각 도서에 대한 모든 학과의 유사도가 다른 도서와 구별되어야 하기 때문임. 
            도서 1에 대해 학과 49개와의 유사도가 ds 리스트의 첫 번째 원소가 되고 도서 2에 대한 모든 학과와의 유사도가 두 번째 원소가 되는 식임''' 
            p = [] # 사후 확률, 즉 최종 점수 리스트임
            for k in range(len(d)):  # 모든 학과에 대하여 시행 
                sd = self.department_names[k]
                r_value = self.rate_table.loc[sd, book_title]
                posterior = (r_value)*(0.1)*d[k]/(10/len(self.department_names)) # 공식에 대입하여 값 구하기
                p.append(posterior) # 최종 점수 리스트에 append
            posteriors.append(p)
        post_arrays = np.array(posteriors) #posteriors를 array형태로 변환. dataframe으로 만들기 위해서임
        return pd.DataFrame(post_arrays, index=self.all_books, columns=self.department_names) # dataframe화하기

    def new_suggest_top_books(self, department, n):
        posteriors = [] # 사후확률. 최종 추천 점수
        print(f'len(all_books)={len(self.all_books)} len(self.department_names)={len(self.department_names)}')
        for i in range(len(self.all_books)): #데이터베이스의 모든 책에 대해 사후 확률 구함
            book_title = self.all_books[i]
            d = self.book_corr[book_title] # 각 도서에 대한 모든 학과의 유사도
            d = d.tolist() #list 형식 변환
            ''''리스트에 추가. 왜냐하면 각 도서에 대한 모든 학과의 유사도가 다른 도서와 구별되어야 하기 때문임. 
            도서 1에 대해 학과 49개와의 유사도가 ds 리스트의 첫 번째 원소가 되고 도서 2에 대한 모든 학과와의 유사도가 두 번째 원소가 되는 식임''' 
            p = [] # 사후 확률, 즉 최종 점수 리스트임
            for k in range(len(d)):  # 모든 학과에 대하여 시행 
                sd = self.department_names[k]
                r_value = self.rate_table.loc[sd, book_title]
                posterior = (r_value)*(0.1)*d[k]/(10/len(self.department_names)) # 공식에 대입하여 값 구하기
                p.append(posterior) # 최종 점수 리스트에 append
            posteriors.append(p)
        post_arrays = np.array(posteriors) #posteriors를 array형태로 변환. dataframe으로 만들기 위해서임
        posterior_dataframe = pd.DataFrame(post_arrays, index=self.all_books, columns=self.department_names) # dataframe화하기
        top_df = posterior_dataframe.sort_values(by=department, ascending=False).head(n)
        print(f'***TOP BOOKS by {department}:')
        print(top_df[department])
        book_titles = top_df.index.tolist()
        books = []
        for sb in book_titles:
            book = self.find_book(sb)
            book['score'] = posterior_dataframe.loc[sb, department]
            book['rate'] = self.rate_table.loc[department, book['title']]
            print('%20s score=%f rate-%f' % ( book['title'], book['score'], book['rate']))
            books.append(book)
        return books

    def new_update_book_score(self, division_name, subdivision_name, department, book, score):
        sd = department
        sb = book['title']
        prev_num = self.num_table.loc[sd, sb]
        self.num_table.loc[sd, sb] += 1
        new_num = self.num_table.loc[sd, sb]
        prev_rate = self.rate_table.loc[sd, sb]
        new_rate = (prev_rate * prev_num + score) / float(new_num)
        self.rate_table.loc[sd, sb] = new_rate
        print(f'score={score} rate_table.loc[{sd}, {sb}]={prev_rate}->{new_rate}, num_table={prev_num}->{new_num}')
        self.save_tables()
        self.save_score_history(division_name, subdivision_name, department, sb, score)

    def save_score_history(self, division_name, subdivision_name, department, book_title, score):
        with open('book_history.txt', 'a') as file:
            line = f'{division_name}, {subdivision_name}, {department}, {book_title}, {score}'
            print('save_score_history(book_history.txt):', line)
            file.write(line + '\n')

    def save_to_csv(self):
        self.books.to_csv(self.csv_path, encoding='euc-kr', index=False)
        print(f'save to {self.csv_path}')

    def set_tables_with_defaults(self):
        default_rate = 0.5
        self.rate_table = pd.DataFrame(index=self.department_names, columns=self.all_books).fillna(default_rate)
        # 건수 테이블
        default_num = 2
        self.num_table = pd.DataFrame(index=self.department_names, columns=self.all_books).fillna(default_num)
        print(f'tables initialized with defaults')

    def save_tables(self):
        self.rate_table.to_csv('rate_table.csv', encoding='euc-kr', index=True, index_label='학과')
        self.num_table.to_csv('num_table.csv', encoding='euc-kr', index=True, index_label='학과')
        print(f'tables saved')

    def load_tables(self):
        self.rate_table = pd.read_csv('rate_table.csv', encoding='cp949', index_col='학과')
        self.num_table = pd.read_csv('num_table.csv', encoding='cp949', index_col='학과')
        print(f'tables loaded')
