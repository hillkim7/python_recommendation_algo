from flask import Flask, send_from_directory, render_template, request, redirect, url_for
from recommendation import *
from books import Books

app = Flask(__name__)
books_ = Books('books.csv')
use_posterior_algo = True

@app.route("/")
def index():
    """ index.html 파일을 브라우저에게 제공한다. """
    return redirect(url_for('page1'))

@app.route("/index.js")
def index_js():
    """ index.js 파일을 브라우저에게 제공한다. """
    return send_from_directory('.', 'index.js')

@app.route("/test.html")
def test_html():
    """ test.html 파일을 브라우저에게 제공한다. """
    return send_from_directory('.', 'test.html')

@app.route("/page1/")
def page1():
    """ page1 파일을 브라우저에게 제공한다. """
    return render_template('page1.html')

@app.route("/page2/")
def page2():
    """ page2 파일을 브라우저에게 제공한다. """
    return render_template('page2.html')

@app.route("/page3/<division>")
def page3(division):
    """ page3 파일을 브라우저에게 제공한다. """
    print('division:', division)
    division = int(division)
    division_name = books_.divisions[division]
    return render_template('page3.html', division=division,
         division_name=division_name)

@app.route("/page4/<division>/<subdivision>")
def page4(division, subdivision):
    """ page4 파일을 브라우저에게 제공한다. """
    division = int(division)
    subdivision = int(subdivision)
    subdivision_name = books_.subdivisions[division][subdivision]
    departments = books_.get_department_names(subdivision_name)
    print(f'page4: {subdivision_name} --> {departments}')
    return render_template('page4.html', division=division,
         subdivision=subdivision, departments=departments)

@app.route("/page5/<division>/<subdivision>/<department>")
def page5(division, subdivision, department):
    division = int(division)
    subdivision = int(subdivision)
    if use_posterior_algo:
        books = books_.new_suggest_top_books(department, 10)
    else:
        books = books_.suggest_top_books(department, 10)
    return render_template('page5.html', division=division,
         subdivision=subdivision, department=department, books=books)

@app.route("/page6/<division>/<subdivision>/<department>/<bookid>/<score>")
def page6(division, subdivision, department, bookid, score):
    division = int(division)
    subdivision = int(subdivision)
    bookid = int(bookid)
    book = books_.get_book(bookid)
    return render_template('page6.html', division=division, subdivision=subdivision, department=department, book=book, score=score)

@app.route("/page6-answer/<division>/<subdivision>/<department>/<bookid>/<score>")
def page6_answer(division, subdivision, department, bookid, score):
    """ page5 파일을 브라우저에게 제공한다. """
    division = int(division)
    subdivision = int(subdivision)
    bookid = int(bookid)
    score = int(score)
    division_name = books_.divisions[division]
    subdivision_name = books_.subdivisions[division][subdivision]
    book = books_.get_book(bookid)
    print(f'page6_answer: {subdivision_name}: bookid={bookid} : {book["title"]} score={score}')
    if use_posterior_algo:
        books_.new_update_book_score(division_name, subdivision_name, department, book, score)
    else:
        books_.update_book_score(division_name, subdivision_name, department, book, score)
    return redirect(url_for('index'))

@app.route("/score_table")
def score_table():
    score_df = books_.get_score_data_frame()
    return score_df.to_html()
@app.route("/score_table.csv")

def score_table_csv():
    score_df = books_.get_score_data_frame()
    score_df.to_csv('score_table.csv', encoding='euc-kr', index=True)
    return send_from_directory('.', 'score_table.csv')

@app.route("/reset_tables")
def reset_tables():
    books_.set_tables_with_defaults()
    books_.save_tables()
    return "reset done!!"

@app.route("/books.csv")
def books_csv():
    return send_from_directory('.', 'books.csv')

@app.route("/rate_table.csv")
def rate_table():
    return send_from_directory('.', 'rate_table.csv')

@app.route("/num_table.csv")
def num_table():
    return send_from_directory('.', 'num_table.csv')

@app.route("/book_history.txt")
def book_history():
    return send_from_directory('.', 'book_history.txt')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)